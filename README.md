# CompArch-MIPS-POWER

This repository contains curriculum materials for a computer architecture class based on the Hennessy & Patterson textbook entitled “Computer Architecture: A Quantitative Approach” and extended to the POWER instruction set architecture (ISA). POWER is an acronym for Performance Optimization With Enhanced RISC.

The structure of the repository is noted below.

•	Lecture Slides
  
    o	Chapter 1: Introduction
  
    o	Chapter 2: Memory Hierarchy
  
    o	Chapter 3: Instruction-Level Parallelism
  
    o	Chapter 4: Data-Level Parallelism 
  
    o	Chapter 5: Thread-Level Parallelism

•	Resources

    o	GEM5 for POWER
  
    o	Microwatt for POWER
  
    o	Forums
  
      *	GEM5
    
      *	Microwatt
    
    o	Tutorials

      *	GEM5
    
      *	Microwatt
    
•	Exercises

Link to other resouces:  https://drive.google.com/drive/folders/1T-pyd9pYG8Hl6lJxHZsKQTKRd2nOP5cZ

Link to the Open Power Foundation compute Hubs where compute resources are available for classroom use:  https://openpowerfoundation.org/hub/
